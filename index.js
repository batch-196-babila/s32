let http = require("http");
const { json } = require("stream/consumers");


let course = [
    {
        name: "Phyton 101",
        description: "Learn Python",
        price: 25000,
    },
    {
        name: "ReactJS 101",
        description: "Learn React",
        price: 35000,
    },
    {
        name: "Express 101",
        description: "Learn ExpressJS",
        price: 28000,
    }
];

http.createServer(function(request, response) {
    //console.log(request.url); //  diff request via endpoints
    //console.log(request.method); // GET method
    /* 
        HTTP requests are differentiated not only via endpoints but also their methods

        HTTP methods simply tells the server what action it must take or what kind of response is needed for the request

        HTTP methods can creare routes with sme endpoint but with different methods
    */
        
        if (request.url === "/" && request.method === "GET") {
            response.writeHead(200, {'Content-type':'text/plain'});
            response.end("This is the / endpoint. GET method request.");
        }
        // browser client is limited to GET  
        // Unable to do POST request
        else if (request.url === "/" && request.method === "POST") {
            response.writeHead(200, {'Content-type':'text/plain'});
            response.end("This is the / endpoint. POST method request.");
        }

        else if (request.url === "/" && request.method === "PUT") {
            response.writeHead(200, {'Content-type':'text/plain'});
            response.end("This is the / endpoint. PUT method request.");
        }

        else if (request.url === "/" && request.method === "DELETE") {
            response.writeHead(200, {'Content-type':'text/plain'});
            response.end("This is the / endpoint. DELETE method request.");
        }

        // Mini Activity

        else if (request.url === "/courses" && request.method === "GET") {
            // application.json is used when sending a JSON format
            response.writeHead(200, {'Content-type':'application/json'});
            response.end(JSON.stringify(course)); 
        }

        else if (request.url === "/courses" && request.method === "POST") {
            // route to add new course, received an input from the client

            // To receive ther request body

            // In NodeJS it's done in 2 steps

            // requestBody will be a placeholder to contain the data (request body) passed from the client

            let requestBody = "";

            /*
                1st Step in receiving data from the request in NodeJS is called data step

                data step - will read the incoming stream of data from client and process it so we can save it un the requestBody variable
            */

            request.on('data', function(data) {
                // console.log(data); // stream of data client
                requestBody += data;
                // data stream is saved into the variable as a string
            })
            console.log(requestBody); // input a black space
            // End Step will run once or after the request data has been completely sent from the client

            request.on('end', function(data) {
                // completely received the data from client and this requestBody now contains our input
                console.log(requestBody); 

                // initially, requestBody is in JSON format. We cannot add this to our courses array because it's a string. So we have to update requestBody variable with a parsed version of received JSON format data

                requestBody = JSON.parse(requestBody);
                //console.log(requestBody);  // data type is onject
                course.push(requestBody);
                console.log(course); // check course array

                response.writeHead(200, {'Content-type':'application/json'});

                // send the updated courses array to the client as JSON format
                response.end(JSON.stringify(course));

            })


        }
}).listen(4000);

console.log("Server running at localhost:4000");